<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class TracksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	$list=[
            [
        	'title' => "Blood on the dance floor",
        	'artist' => "Michael Jackson",
        	'album' => "Blood on the dance floor"
            ],
            [
            'title' => "Billie Jean",
            'artist' => "Michael Jackson",
            'album' => "Thriller"
            ],
            [
                'title' => 'Chinese Democracy',
                'artist' => 'Guns N Roses',
                'album' => 'Chinese Democracy'
            ]
        ];            

        foreach ($list as $item){
            $newtrackObj=new App\Track;
            $newtrackObj->fill($item);
            $newtrackObj->save();
        }
    }
}