<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    public function __construct(){        
    }
    protected $fillable=[
        'id',
        'title',
        'artist',
        'album'
    ];
}
