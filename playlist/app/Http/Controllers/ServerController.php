<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServerController extends Controller
{
    public function gethostname(){
	   	return array('success'=>true,"response"=> gethostname());
    }

    public function sethostname(Request $req){
        $hname=$req->input('hostname');
    	return $hname;
    }

    public function getuptime(){
    	//** double check before deployment
        return array('success'=>true,"response"=> shell_exec("uptime -p"));
    }
}
