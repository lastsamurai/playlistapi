<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Track;

class TrackController extends Controller
{
    public function create(Request $req){
    	$new_track=$req->input();
    	//is it a valid array with appropriate fields?
    	//Sanitize
    	try {

    		$newtrackObj=new Track;
    		$newtrackObj->fill($new_track);
    		$newtrackObj->save();

		} catch (\Illuminate\Database\QueryException $exception) {
		    // we can check get the details of the error using `errorInfo`:
		    $errorInfo = $exception->errorInfo;

		    return array("success"=>false,"response"=>"could not create");
		}

		return array("success"=>true,"response"=>$newtrackObj);

    }
    public function get(Request $req){
    	$id=$req->input('id');
    	//sanitize
    	if($id && $track= Track::find($id))
    		return $resp=array("success"=>true,"response"=>$track);
    	
    		return $resp=array("success"=>false,"response"=>"id not valid");
    }
    public function update(Request $req){
    	$query=$req->input();
    	//santitize query
    	if($query['id'] && $track=Track::find($query['id']))
    		return array("success"=>true, "response"=> $track->update($query) );
    	else
    		return array("success"=>false,"response"=>"Could not find requested record");
    }
    public function delete(Request $req){
    	$query=$req->input();
    	//sanitize query

    	if($query['id'] && $track=Track::find($query['id'])){
    		$track->forceDelete();
    		return array('success'=>true,"response"=>"track id# ".$query['id']." deleted");
    	}else
    		return array('success'=>false,"response"=>"could not delete");
    }
    public function getAll(){//GET
    	return Track::all()->toJson(JSON_PRETTY_PRINT);
    }
}
