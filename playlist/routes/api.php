<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/Hostname.get',"ServerController@gethostname");
Route::get('/Uptime',"ServerController@getuptime");
Route::post('/Hostname.set',"ServerController@sethostname");


Route::post('/Track.create',"TrackController@create");
Route::post('/Track.get',"TrackController@get");
Route::post('/Track.update',"TrackController@update");
Route::post('/Track.delete',"TrackController@delete");

Route::get('/Track.getAll',"TrackController@getAll");
Route::post('/Track.search',"TrackController@search");
