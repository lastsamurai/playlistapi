# Playlist HTTP API Service

This repository is a Dockerized HTTP API Service made up of
* Laravel-powered app
* Mysql database
* NginX web server

it is ready for deployment and 
* docker-compose up * will simply run the entire app.

## Playlist APP

placed within the playlist/ directory, Playlist is a Laravel application that manipulates tracks data in the database and offers CRUD Ops HTTP API.
once running, check out the following API functions:
```bash
POST /api/Track.create
{"title":"Billie Jean", "artist":"Michael Jackson", "album":"thriller"}

POST /api/Track.get
{"id":2}

POST /api/Track.update
{"id":2, "title":"Thriller"}

POST /api/Track.delete
{"id":2}

GET /api/Track.getAll

GET /api/Hostname.get

POST /api/Hostname.set
{"hostname":"localhost"}


```
